Introduction
------------

The Blackfire module helps to integrate your site with the Blackfire.io
performance management tool.

* For a full description of the module, visit the
  [project page](https://www.drupal.org/project/blackfire)

* [To submit bug reports and feature suggestions, or to track changes]
  (https://www.drupal.org/project/issues/2798989)

Requirements
------------

This module does not require any other modules.

Installation
------------

 * Install [as you would normally install a contributed Drupal module]
(https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules).

 * [Set up your site for profiling with Blackfire]
 (https://blackfire.io/docs/up-and-running/installation)

Configuration
-------------

Just install this module, and it will start logging profiles.

Maintainers
-----------

Current maintainers:

 * [Dave Vasilevsky (vasi)](https://www.drupal.org/user/390545)

This project has been sponsored by:

 * [Evolving Web](https://evolvingweb.ca)
